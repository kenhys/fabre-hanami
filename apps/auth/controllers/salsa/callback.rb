require "net/http"
require "securerandom"
require_relative "../../../../lib/fabre/interactors/session/create"
require_relative "../../../../lib/fabre/interactors/user/create"

module Auth
  module Controllers
    module Salsa
      class Callback
        include Auth::Action

        expose :user

        def call(params)
          @client_id = ENV["SALSA_APPLICATION_ID"]
          @client_secret = ENV["SALSA_SECRET_ID"]
          @redirect_uri = ENV["SALSA_REDIRECT_URI"]
          uri = URI.parse("https://salsa.debian.org/oauth/token")
          form_data = {
            "client_id" => @client_id,
            "client_secret" => @client_secret,
            "code" => params[:code],
            "grant_type" => "authorization_code",
            "redirect_uri" => @redirect_uri
          }
          response = Net::HTTP.post_form(uri, form_data)
          case response
          when Net::HTTPSuccess
            json = JSON.parse(response.body)
            @client = Gitlab.client(
              endpoint: 'https://salsa.debian.org/api/v4',
              private_token: json["access_token"]
            )
            uuid = SecureRandom.uuid
            p @client.user
            @user = @client.user

            params = {
              username: @user.username,
              uuid: uuid
            }
            @session_interactor = SessionInteractor::Create.new(params).call
            unless @session_interactor.success?
              status 422, "Unprocessable Entity Session"
            else
              session[:username] = @user.username
              session[:avatar] = @user.avatar_url
            end

            params = {
              username: @user.username,
              avatar: @user.avatar_url
            }
            @user_interactor = UserInteractor::Create.new(params).call
            unless @user_interactor.success?
              status 422, "Unprocessable Entity User"
            end
          else
            status 503, "Service Unavailable"
          end
        end
      end
    end
  end
end
