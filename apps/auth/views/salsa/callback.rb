module Auth
  module Views
    module Salsa
      class Callback
        include Auth::View
        layout :login
      end
    end
  end
end
