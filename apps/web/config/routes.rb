# Configure your routes here
# See: https://guides.hanamirb.org/routing/overview
#
# Example:
# get '/hello', to: ->(env) { [200, {}, ['Hello from Hanami!']] }
get '/', to: 'home#index'

post '/bugs/search', to: 'bugs#search'
get '/bugs/:id(.:comment)', to: 'bugs#show'
get '/signin', to: 'session#signin'
get '/signout', to: 'session#signout'
