module Web
  module Authentication
    def self.included(action)
      action.class_eval do
        before :authenticate!
        expose :current_user
      end
    end

    def authenticated?
      !!current_user
    end

    private

    def authenticate!
      #UserRepository.new.find_by_username_and_uuid(session[:username], session[:uuid])
      authenticated?
    end

    def current_user
      p "A"
      @current_user ||= session[:username]
      @current_user
    end
  end
end
