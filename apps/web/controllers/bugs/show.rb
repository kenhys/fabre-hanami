module Web
  module Controllers
    module Bugs
      class Show
        include Web::Action
        expose :bug_number

        params do
          required(:id).filled(:int?)
        end

        def call(params)
          @bug_number = params[:id]
        end
      end
    end
  end
end
