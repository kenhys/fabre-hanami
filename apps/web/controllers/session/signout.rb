module Web
  module Controllers
    module Session
      class Signout
        include Web::Action

        def call(params)
          session[:username] = nil
          @current_user = nil
        end
      end
    end
  end
end
