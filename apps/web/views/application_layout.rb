module Web
  module Views
    class ApplicationLayout
      include Web::Layout

      def salsa_client_id
        ENV["SALSA_APPLICATION_ID"]
      end

      def salsa_request_uri
        "https://salsa.debian.org/oauth/authorize/"
      end

      def salsa_redirect_uri
        ENV["SALSA_REDIRECT_URI"]
      end

      def salsa_response_type
        "code"
      end

      def salsa_state
        "hoge"
      end

      def salsa_scope
        "read_user"
      end
    end
  end
end
