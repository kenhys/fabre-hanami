module Web
  module Views
    module Session
      class Signout
        include Web::View
        layout :signout
      end
    end
  end
end
