#!/usr/bin/perl

=pod

=head1 NAME

fabre-extract-log.pl


=head1 SYNOPSIS

fabre-extract-log.pl PATH_TO_QUEUE_FILE

fabre-extract-log.pl PATH_TO_DEBBUGS_LOG_FILE PATH_TO_OUTPUT_DIR

=head1 DESCRIPTION

=over

=item PATH_TO_QUEUE_FILE

It specifies the path to queue file. queue file contains
list of log files. The each log file is described in one line.

Here is the example of fabre.queue.txt:

 955594.log
 922696.log
 936196.log
 955497.log
 955099.log

The actual log files are searched under B<FABRE_SPOOL_DIR> environment
variable.

The queue file is generated under B<FABRE_DATA_DIR> environment variable.

=item PATH_TO_DEBBUGS_LOG_FILE

It specifies the full path to log file.
The suffix of file name must be .log.

=item PATH_TO_OUTPUT_DIR

It specifies the directory which saves output file under it.

=back

=head1 AUTHOR

Kentaro Hayashi <kenhys@gmail.com>

=head1 COPYRIGHT & LICENSE

Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=cut

use warnings;
use strict;
use utf8;

use IO::File;
use File::Spec;
use File::Basename;
use Data::Dumper;

use Debbugs::Log;
use Debbugs::MIME;
use Debbugs::CGI::Bugreport;
use JSON;
use Encode;
use Getopt::Long;

sub get_bug_number {
    my $path = shift;
    my ($basename, $dirname, $extention) = fileparse($path, qr/\..+$/);
    return $basename;
};

sub get_suffix {
    my $number = shift;
    $number =~ /.+(\d{2})$/ or die "failed to match suffix of bug number: <$number>";
    return $1;
};

sub parse_summary_file {
    my $path = shift;
    my $summary = Debbugs::Status::read_bug(summary => $path);
    print "||" . $summary->{"done"} . "||\n";
    return $summary;
}

sub get_field {
    my ($entity, $field) = @_;
    my $text = $entity->head->get($field);
    if ($text and length($text) > 0) {
	return $text;
    } else {
	return "(empty)\n";
    }
}

sub parse_log_file {
    my ($path) = @_;
    my $log = Debbugs::Log->new(log_name => $path);
    my @records = $log->read_all_records();
    my %data = ();
    my $msg_number = 0;
    my $skip_next = 0;
    for my $record (@records) {
	$msg_number++;
	if ($skip_next) {
	    $skip_next = 0;
	    next;
	}
	$skip_next = 1 if $record->{type} eq 'html';
	unless ($record->{type} eq 'html') {
	    my $entity = Debbugs::MIME::parse_to_mime_entity($record);
	    print "Messages #$msg_number\n";
	    print "From: " . $entity->head->get("From");
	    print "To: " . $entity->head->get("To");
	    print "Subject: " . get_field($entity, "Subject");
	    print "Date: " . $entity->head->get("Date");
	    my $message = Debbugs::MIME::parse($record->{text});
	    #print join("\n", @{$message->{header}});
	    #print join("\n", @{$message->{body}});
	    my $from = $entity->head->get("From");
	    chomp($from);
	    my $to = $entity->head->get("To");
	    chomp($to);
	    my $subject = get_field($entity, "Subject");
	    chomp($subject);
	    my $date = $entity->head->get("Date");
	    chomp($date);
	    my %comment = (
		'from' => $from,
		'to' => $to,
		'subject' => $subject,
		'date' => $date,
		'header' => join("\n", @{$message->{header}}),
		'body' => join("\n", @{$message->{body}})
		);
	    $data{$msg_number} = \%comment;
	}
    }
    return %data;
};

sub parse_queue_file {
    my ($path, $db_h_dir, $data_dir) = @_;
    open(my $fh, $path) or die "can't open $path: $!";
    my $file_name;
    while (!eof($fh)) {
	chomp($file_name = readline $fh) or die "readline failed for $path: $!";
	process_log_file($file_name, $db_h_dir, $data_dir);
    }
};

sub write_json_file {
    my ($path, $data) = @_;
    open(OUT, ">$path") or die "failed to write $path";
    print OUT encode_json($data);
    close(OUT);
};

sub process_log_file {
    my ($file_name, $db_h_dir, $data_dir) = @_;
    my $bug_number = get_bug_number($file_name);
    my $suffix = get_suffix($bug_number);
    my $file_path = File::Spec->catfile($db_h_dir, $suffix, basename($file_name));
    print "$file_path\n";
    my %data = parse_log_file($file_path);
    #print Dumper %data;

    my $json_path = File::Spec->catfile($data_dir, "$suffix", "$bug_number.json");
    mkdir(dirname($json_path));

    my $summary_file = "$bug_number.summary";
    my $summary_path = File::Spec->catfile($db_h_dir, "$suffix", $summary_file);
    my $summary = parse_summary_file($summary_path);
    my $merged = {bug => $summary, comments => \%data};
    print "$json_path\n";
    write_json_file($json_path, $merged);
};

my $spool_dir = $ENV{FABRE_SPOOL_DIR};
my $data_dir = $ENV{FABRE_DATA_DIR};
my $db_h_dir = File::Spec->catfile($spool_dir, 'db-h');

my $out_dir = "";
my $log_file = "";
my $queue_file = "";

GetOptions("log-file=s" => \$log_file, "queue-file=s" => \$queue_file, "output-dir=s" => \$out_dir);

if ($queue_file && -r $queue_file) {
    my ($base_name, $dir, $suffix) = fileparse($queue_file, qr/\.[^\.]*$/);
    if ($suffix =~ /txt/) {
	parse_queue_file($queue_file, $db_h_dir, $data_dir);
    } else {
	die "--queue-file must be .txt";
    }
} elsif ($log_file && -r $log_file) {
    my ($base_name, $dir, $suffix) = fileparse($log_file, qr/\.[^\.]*$/);
    if ($suffix =~ /log/) {
	process_log_file($log_file, $db_h_dir, $data_dir);
    } else {
	die "--log-file must be .log";
    }
} else {
    die "--log-file or --queue-file must be set";
}
