#!/usr/bin/ruby
#
# Copyright 2020 by Kentaro Hayashi <kenhys@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative "../lib/fabre/converters/grn"
require_relative "../lib/fabre/converters/mailbody"

class Converter
  def convert(path)
    unless path.end_with?(".json")
      puts "Skip converting: <#{path}>"
      return
    end
    base_name = File.basename(path, ".json")
    grn_path = File.join(File.dirname(path), "#{base_name}.grn")
    if File.exist?(grn_path)
      puts "Skip converting: <#{path}>"
      return
    end
    open(grn_path, "w+") do |output|
      options = {
        :debug => false,
        :input => path,
        :output => output
      }
      converter = Fabre::Converters::GrnGenerator.new(options)
      converter.run
    end
  end
end


unless ENV["FABRE_DATA_DIR"]
  puts "ERROR: FABRE_DATA_DIR is empty"
  exit 1
end

converter = Converter.new

if ARGV.size == 0
  Dir.glob("#{ENV['FABRE_DATA_DIR']}/**/*.json") do |path|
    puts "Converting: <#{path}>"
    grn_path = path.sub(/json/, "grn")
    unless File.exist?(grn_path)
      converter.convert(path)
    end
  end
else
  ARGV.each do |path|
    converter.convert(path)
  end
end
