require "json"
require "time"
require "stringio"
require "mail"

module Fabre
  module Converters
    class GrnGenerator
      def initialize(options={})
        @input = options[:input]
        @output = options[:output] || $stdout
        @debug = options[:debug] || false
        @from = []
        @to = []
      end

      def json?(path)
        path.end_with?(".json")
      end

      def comment_orders
        @comments.keys.sort do
          |a, b| a.to_i <=> b.to_i
        end
      end

      def last_modified
        to_bug_time(@headers["last_modified"])
      end

      def reported_date
        to_bug_time(@headers["date"])
      end

      def to_bug_time(unixtime_string)
        # Note that "UTC" causes unexpected error
        Time.at(unixtime_string.to_i, in: "+00:00")
      end

      def comments_fields
        ["comment_number", "from", "to", "subject", "date", "content", "markup"].sort
      end

      def write_load(table)
        @output.puts "\nload --table #{table}"
        @output.puts "["
        yield @output
        @output.puts "]"
      end

      def append_from(key, value)
        if value and not value.empty?
          @from << value
          @buffer << %("#{key}": "#{value}")
        end
      end

      def append_to(key, value)
        if value and not value.empty?
          @to << value
          @buffer << %("#{key}": "#{value}")
        end
      end

      def write_comments
        write_load("Comments") do |output|
          data = []
          comment_orders.each do |comment_number|
            comment = @comments[comment_number]
            @buffer = [%("_key": "#{@bug_number}\##{comment_number}")]
            comments_fields.each do |key|
              case key
              when "comment_number"
                @buffer << %("comment_number": #{comment_number.to_i})
              when "date"
                if comment[key] and not comment[key].empty?
                  unixtime = 0
                  begin
                    unixtime = Time.parse(comment[key]).to_i
                  rescue ArgumentError
                    # force fallback to UTC
                    timestamp = Time.parse(comment[key].sub(/[\+-]\d{4}/, "+00:00Z"))
                    unixtime = timestamp.to_i
                  end
                  puts "#{comment[key]} #{unixtime} #{Time.at(unixtime, in: '+00:00')}" if @debug
                  @buffer << %("date": #{unixtime})
                end
              when "content"
                options = {
                  :debug => @debug,
                  :input => comment['body'],
                  :output => StringIO.new
                }
                mailbody = Fabre::Converters::MailBody.new(options)
                content = mailbody.filter
                unless content.empty?
                  @buffer << %("content": "#{content}")
                end
              when "markup"
              when "from"
                append_from(key, comment[key])
              when "to"
                append_to(key, comment[key])
              else
                if comment[key] and not comment[key].empty?
                  @buffer << %("#{key}": "#{comment[key]}")
                end
              end
            end
            data << sprintf("{%s}", @buffer.join(", "))
          end
          output.puts data.join(",\n")
        end
      end

      def bugs_fields
        [
          "package",
          "subject",
          "originator",
          "created_at",
          "updated_at",
          "severity",
          "done_by"
        ].sort
      end

      def write_bugs
        @originator = @headers["originator"]
        write_load("Bugs") do |output|
          data = {}
          bugs_fields.each do |key|
            case key
            when "created_at"
              data[key] = last_modified
            when "updated_at"
              data[key] = Time.now.getutc
            when "done_by"
              if @headers["done"]
                data[key] = @headers["done"]
                @done_by = @headers["done"]
              end
            else
              data[key] = @headers[key]
            end
          end
          buffer = ["\"_key\": #{@headers['bug_num'].to_i}"]
          data.each do |key,value|
            buffer << "\"#{key}\": \"#{value}\""
          end
          output.printf"{%s}\n", buffer.join(", ")
        end
      end

      def reporter(address)
        data = []
        data << %Q("_key": "#{address.to_s}") unless address.to_s.empty?
        data << %Q("email": "#{address.address}") unless address.address.nil? or address.address.empty?
        data << %Q("display_name": "#{address.display_name}") unless address.display_name.nil? or address.display_name.empty?
        sprintf("{%s}", data.join(", "))
      end

      def write_reporters
        write_load("Reporters") do |output|
          data = []
          if @originator
            data << reporter(Mail::Address.new(@originator))
          end
          if @done_by
            data << reporter(Mail::Address.new(@done_by))
          end
          @from.each do |from|
            next if from.nil? or from.empty?
            data << reporter(Mail::Address.new(from))
          end
          @to.each do |to|
            next if to.nil? or to.empty?
            data << reporter(Mail::Address.new(to))
          end
          output.printf("%s\n", data.join(",\n"))
        end
      end

      def run(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        unless json?(@input)
          raise ArgumentError
        end
        puts "Read: <#{@input}>" if @debug
        open(@input, "r") do |file|
          json = JSON.load(File.read(file))
          @headers = json["bug"]
          @bug_number = @headers["bug_num"]
          @comments = json["comments"]
          @headers["created_at"] = reported_date
          write_bugs
          write_comments
          write_reporters
        end
      end
    end
  end
end
