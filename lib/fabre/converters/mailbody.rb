module Fabre
  module Converters
    class MailBody
      HEADER_PATTERN = /^(([A-Z].+):\s(.+))/

      def initialize(options={})
        @debug = options[:debug] || false
        @input = options[:input] || ""
        @output = options[:output] || $stdout
        @headers = {}
      end

      def filter(options={})
        @input = options[:input] if options[:input]
        @output = options[:output] if options[:output]
        filter_signature
        filter_header
        filter_comment
        @output << @input
        puts "filtered: <#{@output.string}>" if @debug
        @output.string
      end

      def debug?
        @debug and @input.size < 1000
      end

      def has_signature?
        puts "before signature filter: <#{@input}>" if debug?
        index = @input.rindex("\n-- \n", -1)
        return false unless index
        puts "has_signature: <#{index > 0}>" if @debug
        index > 0
      end

      def filter_signature
        index = -1
        if has_signature?
          index = @input.rindex("\n-- \n", -1)
        end
        @input = @input.slice(0..index)
      end

      def has_header?
        puts "before header: <#{@input}>" if debug?
        @last_empty_index = @input.split("\n", 10).index do |line|
          line.empty?
        end
        return false if @last_empty_index.nil?
        unmatch_index = @input.split("\n", @last_empty_index + 2)[0..@last_empty_index - 1].index do |line|
          not line.match?(HEADER_PATTERN)
        end
        unmatch_index.nil?
      end

      def filter_header
        if has_header?
          buffer = []
          @input.split("\n", @last_empty_index + 2)[0..@last_empty_index - 1].each do |line|
            if line.match?(HEADER_PATTERN)
              line.match(HEADER_PATTERN) do |matched|
                @headers[matched[2]] = matched[3]
              end
            end
            buffer << line
          end
          @input.sub!(buffer.join("\n") + "\n\n", "")
        end
        puts "filter header: <#{@input}>" if debug?
      end

      def filter_comment
        buffer = []
        @input.split("\n").each do |line|
          unless line.match?("^>\s.+")
            buffer << line
          end
        end
        last_new_line = @input.split("\n", 2).count < 2 ? "" : "\n"
        @input = buffer.join("\n") + last_new_line
        puts "filter comment: <#{@input}>" if debug?
      end
    end
  end
end
