require "hanami/interactor"
require "hanami/validations"

module SessionInteractor
  class Create
    include Hanami::Interactor

    class Validation
      include Hanami::Validations

      validations do
        required(:username).value(:filled?, :str?)
        required(:uuid).value(:filled?, :str?, size?: 36)
      end
    end

    expose :session

    def initialize(params)
      @params = params
    end

    def call
      now = Time.now.getutc
      @session = SessionRepository.new.create(
        key: @params[:username],
        uuid: @params[:uuid],
        timestamp: now
      )
    end

    private

    def valid?
      @validation = Validation.new(@params).validate
      if @validation.failure?
        error(@validation.messages)
      end
      @validation.success?
    end
  end
end
