require "hanami/interactor"
require "hanami/validations"

module UserInteractor
  class Create
    include Hanami::Interactor

    class Validation
      include Hanami::Validations

      validations do
        required(:username).value(:filled?, :str?)
        required(:avatar).value(:filled?, :str?)
      end
    end

    expose :user

    def initialize(params)
      @params = params
    end

    def call
      @user = UserRepository.new.create(
        key: @params[:username],
        avatar: @params[:avatar]
      )
    end

    private

    def valid?
      @validated = Validation.new(@params).validate
      if @validated.failure?
        error(@validated.messages)
      end
      @validated.success?
    end
  end
end
