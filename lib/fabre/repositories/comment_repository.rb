require_relative "repository"

class CommentRepository < Fabre::Repository
  def create(data={})
  end

  def update(key, data={})
  end

  def delete(key)
  end

  def all
  end

  def key?(key_or_id)
    not key_or_id.number?
  end

  def id?(key_or_id)
    key_or_id.number?
  end

  def find(key_or_id)
    if key_or_id.number?
      find_by_number(key_or_id)
    else
      find_by_key(key_or_id)
    end
  end

  def find_by_key(key)
  end

  def find_by_id(id)
  end

  def first
  end

  def last
  end

  def clear
  end
end
