require "groonga"

module Fabre
  class Repository
    def initialize(options={})
      @database_path = File.join(
        File.dirname(__FILE__),
        "../../..",
        ENV["GROONGA_DB"]
      )
      @database_path = options[:database_path] if options[:database_path]
      ::Groonga::Context.default_options = { encoding: :utf8 }
      ::Groonga::Database.open(File.expand_path(@database_path))
    end
  end
end
