require_relative "repository"

class UserRepository < Fabre::Repository
  def initialize(options={})
    super(options)
    @users = Groonga["Users"]
  end

  def create(data={})
    now = Time.now.getutc
    unless @users.has_key?(data[:key])
      @users.add(data[:key],
                 :avatar => data[:avatar],
                 :created_at => now,
                 :updated_at => now)
    else
      update(data[:key], data)
    end
  end

  def update(key, data={})
    data.keys.each do |column|
      case column
      when :avatar
        @users[key].avatar = data[column]
      when :like
        @users[key].like = data[column]
      when :bookmark
        @users[key].bookmark = data[column]
      when :updated_at
        @users[key].updated_at = data[column]
      end
    end
  end

  def delete(key)
    @users[key].delete
  end

  def all
    @users.all
  end

  def key?(key_or_id)
    not key_or_id.number?
  end

  def id?(key_or_id)
    key_or_id.number?
  end

  def find(key_or_id)
    if key_or_id.number?
      find_by_number(key_or_id)
    else
      find_by_key(key_or_id)
    end
  end

  def find_by_key(key)
  end

  def find_by_id(id)
  end

  def first
  end

  def last
  end

  def clear
  end

  def success?
  end
end
