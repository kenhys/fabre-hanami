require "test/unit"

class TestGrnGenerator < Test::Unit::TestCase

  def setup
    @output = StringIO.new
    @generator = Fabre::Converters::GrnGenerator.new
  end

  def generate_json(data)
    Dir.mktmpdir do |dir|
      json_path = "#{dir}/comments.json"
      File.open(json_path, "w+") do |file|
        file.puts data
        file.rewind
        yield json_path
      end
    end
  end

  def generate(fixture_name)
    @generator.run({
                     :input => fixture_json_path(fixture_name),
                     :output => @output
                   })
  end

  def test_comment_orders
    generate("comments_order")
    assert_equal(["1", "10"], @generator.comment_orders)
  end

  def test_bug305913_null_date
    generate("bug305913_null_date")
    expected = fixture_grn("bug305913_null_date")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_bug67606_invalid_utc_offset
    generate("bug67606_invalid_utc_offset")
    expected = fixture_grn("bug67606_invalid_utc_offset")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_bug28207_empty_date
    generate("bug28207_empty_date")
    expected = fixture_grn("bug28207_empty_date")
    assert_equal(expected, filter_grn(@output.string))
  end

  def test_grn
    expected = fixture_grn("comments_order")
    generate("comments_order")
    assert_equal(expected, filter_grn(@output.string))
  end
end
