require "test/unit"

class TestMailBody < Test::Unit::TestCase

  def setup
    @mailbody = Fabre::Converters::MailBody.new
    @output = StringIO.new
  end

  def filter(data, expected)
    @mailbody.filter({
                       :input => data,
                       :output => @output
                     })
    assert_equal(expected, @output.string)
  end

  def test_no_header
    data = <<"EOS"
This is no header
EOS
    filter(data, data)
  end

  def test_one_header
    data = <<"EOS"
Version: 1.0

This is one header
EOS
    expected = <<"EOS"
This is one header
EOS
    filter(data, expected)
  end

  def test_multiple_headers
    data = <<"EOS"
Version: 1.0
Package: wnpp

There is multiple headers.
EOS
    expected = <<"EOS"
There is multiple headers.
EOS
    filter(data, expected)
  end

  def test_one_signature
    data = <<"EOS"
Regards,

-- 
Anonymous coward
EOS
    expected = <<"EOS"
Regards,
EOS
    filter(data, expected)
  end

  def test_one_comment
    data = <<"EOS"

Alice :
> a
> b
> c

This is body
EOS
    expected = <<"EOS"

Alice :

This is body
EOS
    filter(data, expected)
  end

  def test_multiple_comments
    data = <<"EOS"

Alice :
> a
> b
> c

This is body

Bob wrote:
> d
> e
> f

Thanks
EOS
    expected = <<"EOS"

Alice :

This is body

Bob wrote:

Thanks
EOS
    filter(data, expected)
  end
end
  
