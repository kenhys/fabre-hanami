ENV["HANAMI_ENV"] ||= "test"

require_relative "../config/environment"
require "test/unit"
require "test/unit/rr"
require "test/unit/capybara"

class Test::Unit::TestCase
  include Capybara::DSL
end

def fixture_json_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "#{name}.json")
end

def fixture_json(name)
  open(File.join(File.dirname(__FILE__), "fixtures", "#{name}.json")) do |file|
    return JSON.load(File.read(file))
  end
end

def fixture_grn_path(name)
  File.join(File.dirname(__FILE__), "fixtures", "#{name}.grn")
end

def fixture_grn(name)
  open(File.join(File.dirname(__FILE__), "fixtures", "#{name}.grn")) do |file|
    return File.read(file)
  end
end

def filter_grn(data)
  data.gsub(/\"updated_at\": \"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} UTC\"/, %Q("updated_at": "2020-01-01 00:00:00 UTC"))
end

Hanami.boot
